# README #
List of some interesting image processing and geosaptial libraries/tutorials

### Geospatial Technologies ###

* [Learn Markdown](https://bitbucket.org)

### Image Processing ###

* [Scikit-Image image processing tutorials](http://scikit-image.org/docs/dev/auto_examples/)
* [OpenCV-Image image processing tutorials](http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_tutorials.html)
* [GIS Ag MAP DOS tutorials](http://www.gisagmaps.com/atco-guide/)
* [GIS Ag MAP Landsat Correlations](http://www.gisagmaps.com/landsat-correlation-links/)

### Vector Processing ###

* [Vector Data Processing using Python Tools](https://geohackweek.github.io/vector/) 
* [Dissolving Polygon](https://gis.stackexchange.com/questions/149959/dissolving-polygons-based-on-attributes-with-python-shapely-fiona)

### Machine Learning ###

* [Sklearn tutorial](https://github.com/jakevdp/sklearn_tutorial/tree/master/notebooks)


### Data Science ###

* [Python DataScience handbook](https://github.com/jakevdp/PythonDataScienceHandbook)


### Python ###

* [Using pandas and scikit-learn for classification tasks DePy 2015](https://github.com/jseabold/depy)